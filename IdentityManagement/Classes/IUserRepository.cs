﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IdentityManagement.Models;

namespace IdentityManagement
{
    public interface IUserRepository
    {
        bool ValidateCredentials(string username, string password);
        User FindBySubjectId(string subjectId);
        User FindByUsername(string username);
    }
}
