﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using IdentityManagement.Models;

namespace IdentityManagement
{
    public class UserRepository : IUserRepository
    {
        // some dummy data. Replce this with your user persistence. 
        //private readonly List<User> _users = new List<User>
        //{
        //    new User{
        //        SubjectId = "123",
        //        UserName = "alice",
        //        Password = "password",
        //        Email = "alice@email.aaa"
        //    },
        //    new User{
        //        SubjectId = "124",
        //        UserName = "alice2",
        //        Password = "password2",
        //        Email = "alice2@email.bbb"                
        //    },
        //};

        private readonly List<User> _users = null;

        //public UserRepository(Action<UserStoreOptions> storeOptionsAction)
        public UserRepository()
        {
            AFT_IdentityContext identityDbContext = new AFT_IdentityContext();
            _users = identityDbContext.Users.ToList();
            //_users = identityDbContext.Users.ToList<User>();
            //string connStr = Startup.GetDefaultConnectionString();            
        }

        public User FindBySubjectId(string subjectId)
        {
            return _users.FirstOrDefault(x => x.Id == int.Parse(subjectId));
        }

        public User FindByUsername(string username)
        {
            return _users.FirstOrDefault(x => x.LoginId.Equals(username, StringComparison.OrdinalIgnoreCase));
        }

        public bool ValidateCredentials(string username, string password)
        {
            var user = FindByUsername(username);
            if (user != null)
            {
                return user.Password.Equals(password);
            }

            return false;
        }
    }
}
