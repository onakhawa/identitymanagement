﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace IdentityManagement
{
    public class UserStoreOptions
    {
        public UserStoreOptions(){}

        //
        // Summary:
        //     Callback to configure the EF DbContext.
        public Action<DbContextOptionsBuilder> ConfigureDbContext { get; set; }
    }
}
