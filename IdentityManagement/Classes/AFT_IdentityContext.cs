﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using IdentityManagement.Models;

namespace IdentityManagement
{
    public partial class AFT_IdentityContext : DbContext
    {
        public AFT_IdentityContext()
        {
        }

        public AFT_IdentityContext(DbContextOptions<AFT_IdentityContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Field> Fields { get; set; }
        public virtual DbSet<GroupFieldRelationship> GroupFieldRelationship { get; set; }
        public virtual DbSet<GroupFieldRelationshipPartyExceptions> GroupFieldRelationshipPartyExceptions { get; set; }
        public virtual DbSet<GroupFieldRelationshipUserExceptions> GroupFieldRelationshipUserExceptions { get; set; }
        public virtual DbSet<Group> Groups { get; set; }
        public virtual DbSet<RoleGroupRelationship> RoleGroupRelationship { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<UserPartyRelationship> UserPartyRelationship { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                string connStr = Startup.GetDefaultConnectionString();                
                optionsBuilder.UseSqlServer(connStr);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Field>(entity =>
            {
                entity.ToTable("fields");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.FieldKeyName)
                    .IsRequired()
                    .HasColumnName("field_key_name")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FieldName)
                    .IsRequired()
                    .HasColumnName("field_name")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<GroupFieldRelationship>(entity =>
            {
                entity.ToTable("group_field_relationship");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CreatedByUserId).HasColumnName("created_by_user_id");

                entity.Property(e => e.FieldId).HasColumnName("field_id");

                entity.Property(e => e.GroupId).HasColumnName("group_id");

                entity.Property(e => e.Permissions)
                    .IsRequired()
                    .HasColumnName("permissions")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.UpdatedByUserId).HasColumnName("updated_by_user_id");

                entity.HasOne(d => d.Field)
                    .WithMany(p => p.GroupFieldRelationship)
                    .HasForeignKey(d => d.FieldId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_group_field_relationship_fields");

                entity.HasOne(d => d.Group)
                    .WithMany(p => p.GroupFieldRelationship)
                    .HasForeignKey(d => d.GroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_group_field_relationship_groups");
            });

            modelBuilder.Entity<GroupFieldRelationshipPartyExceptions>(entity =>
            {
                entity.ToTable("group_field_relationship_party_exceptions");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasColumnType("datetime");

                entity.Property(e => e.CreatedByUserId).HasColumnName("created_by_user_id");

                entity.Property(e => e.FieldId).HasColumnName("field_id");

                entity.Property(e => e.GroupId).HasColumnName("group_id");

                entity.Property(e => e.PartyId).HasColumnName("party_id");

                entity.Property(e => e.Permissions)
                    .IsRequired()
                    .HasColumnName("permissions")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasColumnType("datetime");

                entity.Property(e => e.UpdatedByUserId).HasColumnName("updated_by_user_id");

                entity.HasOne(d => d.Field)
                    .WithMany(p => p.GroupFieldRelationshipPartyExceptions)
                    .HasForeignKey(d => d.FieldId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_group_field_relationship_party_exceptions_fields");

                entity.HasOne(d => d.Group)
                    .WithMany(p => p.GroupFieldRelationshipPartyExceptions)
                    .HasForeignKey(d => d.GroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_group_field_relationship_party_exceptions_groups");
            });

            modelBuilder.Entity<GroupFieldRelationshipUserExceptions>(entity =>
            {
                entity.ToTable("group_field_relationship_user_exceptions");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasColumnType("datetime");

                entity.Property(e => e.CreatedByUserId).HasColumnName("created_by_user_id");

                entity.Property(e => e.FieldId).HasColumnName("field_id");

                entity.Property(e => e.GroupId).HasColumnName("group_id");

                entity.Property(e => e.PartyId).HasColumnName("party_id");

                entity.Property(e => e.Permissions)
                    .IsRequired()
                    .HasColumnName("permissions")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasColumnType("datetime");

                entity.Property(e => e.UpdatedByUserId).HasColumnName("updated_by_user_id");

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.HasOne(d => d.Field)
                    .WithMany(p => p.GroupFieldRelationshipUserExceptions)
                    .HasForeignKey(d => d.FieldId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_group_field_relationship_user_exceptions_fields");

                entity.HasOne(d => d.Group)
                    .WithMany(p => p.GroupFieldRelationshipUserExceptions)
                    .HasForeignKey(d => d.GroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_group_field_relationship_user_exceptions_groups");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.GroupFieldRelationshipUserExceptions)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_group_field_relationship_user_exceptions_users");
            });

            modelBuilder.Entity<Group>(entity =>
            {
                entity.ToTable("groups");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.GroupName)
                    .IsRequired()
                    .HasColumnName("group_name")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<RoleGroupRelationship>(entity =>
            {
                entity.ToTable("role_group_relationship");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CreatedByUserId).HasColumnName("created_by_user_id");

                entity.Property(e => e.GroupId).HasColumnName("group_id");

                entity.Property(e => e.Priority).HasColumnName("priority");

                entity.Property(e => e.RoleId).HasColumnName("role_id");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.UpdatedByUserId).HasColumnName("updated_by_user_id");

                entity.HasOne(d => d.Group)
                    .WithMany(p => p.RoleGroupRelationship)
                    .HasForeignKey(d => d.GroupId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_role_group_relationship_groups");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.RoleGroupRelationship)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_role_group_relationship_roles");
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.ToTable("roles");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.RoleName)
                    .IsRequired()
                    .HasColumnName("role")
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<UserPartyRelationship>(entity =>
            {
                entity.ToTable("user_party_relationship");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasColumnType("datetime");

                entity.Property(e => e.CreatedByUserId).HasColumnName("created_by_user_id");

                entity.Property(e => e.PartyId).HasColumnName("party_id");

                entity.Property(e => e.RoleId).HasColumnName("role_id");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasColumnType("datetime");

                entity.Property(e => e.UpdatedByUserId).HasColumnName("updated_by_user_id");

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.UserPartyRelationship)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_user_party_relationship_roles");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserPartyRelationship)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_user_party_relationship_users");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("users");

                entity.HasIndex(e => e.LoginId)
                    .HasName("IX_users")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.CreatedByUserId).HasColumnName("created_by_user_id");

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasColumnName("first_name")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.IndividualPartyId).HasColumnName("individual_party_id");

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasColumnName("last_name")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.LoginId)
                    .IsRequired()
                    .HasColumnName("login_id")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.MiddleName)
                    .HasColumnName("middle_name")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .HasColumnName("password")
                    .HasMaxLength(250)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateAt)
                    .HasColumnName("update_at")
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.UpdatedByUserId).HasColumnName("updated_by_user_id");
            });
        }
    }
}
