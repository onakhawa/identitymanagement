﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using IdentityManagement.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;
using IdentityServer4.EntityFramework.Mappers;
using IdentityServer4.EntityFramework.DbContexts;
using Microsoft.Extensions.Logging;

namespace IdentityManagement
{
    public class Startup
    {
        public static string ConnectionString { get; private set; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            string identityConnectionString = Configuration.GetConnectionString("AFTIdentityConnection");
            Startup.ConnectionString = identityConnectionString;
            string migrationAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;
            //////services.Configure<CookiePolicyOptions>(options =>
            //////{
            //////    // This lambda determines whether user consent for non-essential cookies is needed for a given request.
            //////    options.CheckConsentNeeded = context => true;
            //////    options.MinimumSameSitePolicy = SameSiteMode.None;
            //////});

            //////services.AddDbContext<ApplicationDbContext>(options =>
            //////    options.UseSqlServer(
            //////        Configuration.GetConnectionString(identityConnectionString)));

            /////services.AddDefaultIdentity<IdentityUser>()
            //////    .AddEntityFrameworkStores<ApplicationDbContext>();

            //////services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            //AddDeveloperSigningCredential() Creates temporary key material for signing tokens. Again this might be useful to get started, but needs to be replaced by some persistent key material for production scenarios.
            
            services.AddDbContext<AFT_IdentityContext>(options =>
                options.UseSqlServer(
                Configuration.GetConnectionString(identityConnectionString)));

            services.AddIdentityServer()
                .AddDeveloperSigningCredential()
                
                .AddCustomUserStore(options =>
                    options.ConfigureDbContext = builder =>
                        builder.UseSqlServer(identityConnectionString))
                //.AddTestUsers(Config.GetUsers())
                //.AddAspNetIdentity<IdentityUser>()
                
                //adds the config data from DB (clients, resources)                
                .AddConfigurationStore(options =>
                    {
                        options.ConfigureDbContext = builder =>
                            builder.UseSqlServer(identityConnectionString, 
                                sql =>  sql.MigrationsAssembly(migrationAssembly));
                    }
                )
                //adds the operational data from DB (codes, tokens, consents)
                .AddOperationalStore(options =>
                    {
                        options.ConfigureDbContext = builder =>
                                builder.UseSqlServer(identityConnectionString,
                                    sql => sql.MigrationsAssembly(migrationAssembly));

                        //this enables automatic token cleanup. this is optional.
                        options.EnableTokenCleanup = true;
                        options.TokenCleanupInterval = 3600; //In seconds                        
                    }
                );
                //.AddInMemoryIdentityResources(Config.GetIdentityResources())
                //.AddInMemoryApiResources(Config.GetApiResources())
                //.AddInMemoryClients(Config.GetClients());
                
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            // this will do the initial DB population
            //InitializeDatabase(app);
            
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                //app.UseDatabaseErrorPage();
            }
            
            //////else
            //////{
            //////    app.UseExceptionHandler("/Home/Error");
            //////    app.UseHsts();
            //////}

            //////app.UseHttpsRedirection();
            //////app.UseStaticFiles();
            //////app.UseCookiePolicy();
            
            app.UseIdentityServer();
            
            //////app.UseMvc(routes =>
            //////{
            //////    routes.MapRoute(
            //////        name: "default",
            //////        template: "{controller=Home}/{action=Index}/{id?}");
            //////});
        }

        public static string GetDefaultConnectionString()
        {
            return Startup.ConnectionString;
        }

        private void InitializeDatabase(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                serviceScope.ServiceProvider.GetRequiredService<PersistedGrantDbContext>().Database.Migrate();

                var context = serviceScope.ServiceProvider.GetRequiredService<ConfigurationDbContext>();
                context.Database.Migrate();
                if (!context.Clients.Any())
                {
                    foreach (var client in Config.GetClients())
                    {
                        context.Clients.Add(client.ToEntity());
                    }
                    context.SaveChanges();
                }

                if (!context.IdentityResources.Any())
                {
                    foreach (var resource in Config.GetIdentityResources())
                    {
                        context.IdentityResources.Add(resource.ToEntity());
                    }
                    context.SaveChanges();
                }

                if (!context.ApiResources.Any())
                {
                    foreach (var resource in Config.GetApiResources())
                    {
                        context.ApiResources.Add(resource.ToEntity());
                    }
                    context.SaveChanges();
                }
            }
        }
    }
}
