﻿using System;
using System.Collections.Generic;

namespace IdentityManagement.Models
{
    public partial class Group
    {
        public Group()
        {
            GroupFieldRelationship = new HashSet<GroupFieldRelationship>();
            GroupFieldRelationshipPartyExceptions = new HashSet<GroupFieldRelationshipPartyExceptions>();
            GroupFieldRelationshipUserExceptions = new HashSet<GroupFieldRelationshipUserExceptions>();
            RoleGroupRelationship = new HashSet<RoleGroupRelationship>();
        }

        public int Id { get; set; }
        public string GroupName { get; set; }

        public ICollection<GroupFieldRelationship> GroupFieldRelationship { get; set; }
        public ICollection<GroupFieldRelationshipPartyExceptions> GroupFieldRelationshipPartyExceptions { get; set; }
        public ICollection<GroupFieldRelationshipUserExceptions> GroupFieldRelationshipUserExceptions { get; set; }
        public ICollection<RoleGroupRelationship> RoleGroupRelationship { get; set; }
    }
}
