﻿using System;
using System.Collections.Generic;

namespace IdentityManagement.Models
{
    public partial class RoleGroupRelationship
    {
        public int Id { get; set; }
        public int RoleId { get; set; }
        public int GroupId { get; set; }
        public int Priority { get; set; }
        public int CreatedByUserId { get; set; }
        public DateTime CreatedAt { get; set; }
        public int UpdatedByUserId { get; set; }
        public DateTime UpdatedAt { get; set; }

        public Group Group { get; set; }
        public Role Role { get; set; }
    }
}
