﻿using System;
using System.Collections.Generic;

namespace IdentityManagement.Models
{
    public partial class GroupFieldRelationship
    {
        public int Id { get; set; }
        public int GroupId { get; set; }
        public int FieldId { get; set; }
        public string Permissions { get; set; }
        public int CreatedByUserId { get; set; }
        public DateTime CreatedAt { get; set; }
        public int UpdatedByUserId { get; set; }
        public DateTime UpdatedAt { get; set; }

        public Field Field { get; set; }
        public Group Group { get; set; }
    }
}
