﻿using System;
using System.Collections.Generic;

namespace IdentityManagement.Models
{
    public partial class UserPartyRelationship
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int PartyId { get; set; }
        public int RoleId { get; set; }
        public int CreatedByUserId { get; set; }
        public DateTime CreatedAt { get; set; }
        public int? UpdatedByUserId { get; set; }
        public DateTime? UpdatedAt { get; set; }

        public Role Role { get; set; }
        public User User { get; set; }
    }
}
