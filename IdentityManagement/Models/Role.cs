﻿using System;
using System.Collections.Generic;

namespace IdentityManagement.Models
{
    public partial class Role
    {
        public Role()
        {
            RoleGroupRelationship = new HashSet<RoleGroupRelationship>();
            UserPartyRelationship = new HashSet<UserPartyRelationship>();
        }

        public int Id { get; set; }
        public string RoleName { get; set; }

        public ICollection<RoleGroupRelationship> RoleGroupRelationship { get; set; }
        public ICollection<UserPartyRelationship> UserPartyRelationship { get; set; }
    }
}
