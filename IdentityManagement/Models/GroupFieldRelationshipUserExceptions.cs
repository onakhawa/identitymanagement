﻿using System;
using System.Collections.Generic;

namespace IdentityManagement.Models
{
    public partial class GroupFieldRelationshipUserExceptions
    {
        public int Id { get; set; }
        public int PartyId { get; set; }
        public int UserId { get; set; }
        public int GroupId { get; set; }
        public int FieldId { get; set; }
        public string Permissions { get; set; }
        public int CreatedByUserId { get; set; }
        public DateTime CreatedAt { get; set; }
        public int UpdatedByUserId { get; set; }
        public DateTime UpdatedAt { get; set; }

        public Field Field { get; set; }
        public Group Group { get; set; }
        public User User { get; set; }
    }
}
