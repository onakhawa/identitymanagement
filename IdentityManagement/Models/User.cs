﻿using System;
using System.Collections.Generic;

namespace IdentityManagement.Models
{
    public partial class User
    {
        public User()
        {
            GroupFieldRelationshipUserExceptions = new HashSet<GroupFieldRelationshipUserExceptions>();
            UserPartyRelationship = new HashSet<UserPartyRelationship>();
        }

        public int Id { get; set; }
        public string LoginId { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public int? IndividualPartyId { get; set; }
        public int CreatedByUserId { get; set; }
        public DateTime CreatedAt { get; set; }
        public int? UpdatedByUserId { get; set; }
        public DateTime? UpdateAt { get; set; }

        public ICollection<GroupFieldRelationshipUserExceptions> GroupFieldRelationshipUserExceptions { get; set; }
        public ICollection<UserPartyRelationship> UserPartyRelationship { get; set; }
    }
}
