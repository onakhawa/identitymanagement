﻿using System;
using System.Collections.Generic;

namespace IdentityManagement.Models
{
    public partial class Field
    {
        public Field()
        {
            GroupFieldRelationship = new HashSet<GroupFieldRelationship>();
            GroupFieldRelationshipPartyExceptions = new HashSet<GroupFieldRelationshipPartyExceptions>();
            GroupFieldRelationshipUserExceptions = new HashSet<GroupFieldRelationshipUserExceptions>();
        }

        public int Id { get; set; }
        public string FieldName { get; set; }
        public string FieldKeyName { get; set; }

        public ICollection<GroupFieldRelationship> GroupFieldRelationship { get; set; }
        public ICollection<GroupFieldRelationshipPartyExceptions> GroupFieldRelationshipPartyExceptions { get; set; }
        public ICollection<GroupFieldRelationshipUserExceptions> GroupFieldRelationshipUserExceptions { get; set; }
    }
}
